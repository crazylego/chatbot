'''
Classe do usuario que vai usar o servico de bot
Dados do usuario:
    Nome = Nome do usuario 
    id = Id do telegram
    Indice = O quanto o usuario ta ficando insatisfeito com o bot
    varia de 1 a 5
'''

from nltk.tree import *
import nltk
from bancoRemedios import Banco
from unicodedata import normalize
from weatherbot import Weather

class User(object):

    def __init__(self, idTelegram, userName):
        self.id = idTelegram
        self.ind = 0 #indice de fadiga do usuario
        self.nome = userName
        self.wordshello = ["ola","bom dia","oi","boa noite","boa tarde"]
        self.wordsweather = ["tempo", "temperatura"]
        self.wordsFarmacia = ["preço","custa"]
        self.banco = Banco('root','1234','127.0.0.1','lab')
    

    def parseFrase(self, frase):
        from pickle import load
        input = open('mac_morpho.pkl', 'rb')
        tagger = load(input)
        input.close()
        tokens = nltk.word_tokenize(frase)
        tagged = tagger.tag(tokens)

        gramatica = r"""
            N0:{<N>|<NPROP>}
        """
        cp = nltk.RegexpParser(gramatica)
        arvore = cp.parse(tagged)
        padrao_NE0 = self.ExtractPhrases(arvore,"N0")
        return padrao_NE0

    def ExtractPhrases(self, myTree, phrase):
        myPhrases = []
        if (myTree.label() == phrase):
            treeTmp = myTree.copy(True)
            word=""
            for w in treeTmp.leaves():
                if (len(word)==0):
                    word = w[0]
                else:
                    word = word+" "+w[0]
            myPhrases.append(word)
        for child in myTree:
            if (type(child) is Tree):
                list_of_phrases = self.ExtractPhrases(child, phrase)
                if (len(list_of_phrases) > 0):
                    myPhrases.extend(list_of_phrases)
        return myPhrases

    def analizaFrase(self, message):
        text = self.remover_acentos(message.text).lower()
        for frases in self.wordshello:
            if(text == frases):
                return self.responde(1,message.text)
        for frases in self.wordsweather:
            if(frases in text):
                return self.responde(2,message.text)
        for frases in self.wordsFarmacia:
                if(frases in text):
                    print(self.banco.pesquisaBanco("epocler"))
        print(text)
        return "Não entendi o que foi dito"

    def remover_acentos(self, txt):
        return normalize('NFKD', txt).encode('ASCII', 'ignore').decode('ASCII')

    def responde(self, index, message):
        if(index == 1):
            return "Bem vindo " + self.nome + "!!\nEm que posso ajudar voce ?"
        elif(index == 2):
            cidades = self.parseFrase(message)
            tempo = Weather()
            for cidade in cidades:
                try:
                    resultado = tempo.getWeather(cidade)
                    return(resultado)
                except ValueError:
                    print(cidade)
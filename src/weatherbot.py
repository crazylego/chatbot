import urllib.request
import xml.etree.ElementTree
import sys
import unicodedata
import datetime

class Weather(object):

    def __init__(self):
        pass

    def getxmlcodes(self, args):
        # Busca do código das cidades
        codigos = []
        with urllib.request.urlopen('http://servicos.cptec.inpe.br/XML/listaCidades?city={0}'.format(args)) as url:
                content = url.read().decode('iso-8859-1')

        root = xml.etree.ElementTree.fromstring(content)
        codigos.extend([ elem.text for elem in root.findall('./cidade/id') ])

        if len(codigos) == 0:
            raise ValueError("A busca não retornou nenhuma cidade")

        return codigos

    def getWeather(self, args):
    # Formatar entrada, remover acentos e substituir espaço por %20
        #args = ["Teresina"]
    # Obter XML das cidades
        for codes in self.getxmlcodes(args):
            with urllib.request.urlopen('http://servicos.cptec.inpe.br/XML/cidade/{0}/previsao.xml'.format(codes)) as url:
                content = url.read().decode('iso-8859-1')

            # Filtrar os dados
            root = xml.etree.ElementTree.fromstring(content)
            dias = [ elem.text for elem in root.findall('previsao/dia') ]
            dias = [ datetime.datetime.strptime(elem, '%Y-%m-%d').strftime('%d/%m/%Y') for elem in dias ]
            clima = [elem.text for elem in root.findall('previsao/tempo') ]
            temperaturas = [ (ma, mi) for ma, mi in zip([elem.text for elem in root.findall('previsao/maxima') ],
                                                        [elem.text for elem in root.findall('previsao/minima') ]) ]

            iuv = [ elem.text for elem in root.findall('previsao/iuv') ]

            # Imprimir resultado
            print('\n\nPrevisão do tempo para {0} - {1}:'.format(root[0].text, root[1].text))

            i = 0
    #       for i in range(len(dias)):
            print('Dia {0}:'.format(dias[i]))
            #print('Clima: {0}'.format(TEMPO[clima[i]]))
            print('Temperatura máxima: {0} °C'.format(temperaturas[i][0]))
            print('Temperatura mínima: {0} °C'.format(temperaturas[i][1]))
            print('Índice UV: {0}'.format(iuv[i]))
            print()
            return "Previsão do tempo para " + root[0].text + "\nTemperatura máxima: "+temperaturas[i][0]+" °C" + "\nTemperatura mínima: "+temperaturas[i][1]+" °C"
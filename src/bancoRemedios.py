import mysql.connector
'''
  Class to manipulate database
'''

class Banco:

    def __init__(self, user, password, host, database):
        self.user = user
        self.password = password
        self.host = host
        self.database = database

    def bdOpenConnection(self):
        cnx = mysql.connector.connect(
            user=self.user,
            password=self.password,
            host=self.host,
            database=self.database
        )
        return cnx

    def pesquisaBanco(self, name):
        cnx = self.bdOpenConnection()
        cursor = cnx.cursor()
        query = "select preco from remedio where nome like '%" + name + "%'"
        print(query)
        cursor.execute(query)
        dale = cursor.fetchall()[0]
        return dale

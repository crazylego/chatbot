-- Table of medicines
CREATE TABLE remedio (
    cod INTEGER(4) AUTO_INCREMENT NOT NULL,
    nome VARCHAR(40) NOT NULL,
    preco FLOAT(5) NOT NULL,
    descp VARCHAR(150),
    PRIMARY KEY(cod)
);